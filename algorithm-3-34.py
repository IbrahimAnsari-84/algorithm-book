import numpy as np
a=[]
n=int(input('enter n '))
for i in range (n):
    #this loop make a n*n matrix and put 0 in all indexes
    c=[]
    for j in range (n):
        c.append(0)
    a.append(c)
for i in range(n):
    #this loop put 1 instead of 0 in some indexes
    for j in range(n):
        if i == j or a[i][j-1]==1 :
            a[i][j] = 1
print(np.array(a))
