#this will take a list of n number and arrange them from biggest to smallest
def listing (n):
    #it gets a list with n sentences
    nums = []
    for i in range (n):
        nums.append(int(input('enter a number: ')))
    return nums

def arrange (nums,n):
    i=0
    while i < n:
        #this while loop iterates list sentences
        for p in range (i,0,-1):
            #this for loop iterates list from sentence i to zero and if sentence p > p-1 value of these sentences changes
            if nums[p] > nums[p-1]:
                nums[p],nums[p-1] = nums[p-1],nums[p]
            else:
                break
        i += 1
    print(nums)
if __name__ == "__main__":
    n = int(input('how many numbers do you want to enter? '))
    arrange(listing(n),n)
