#this gets a matrix and prints two listed that every sentence of them is sum of a column or a row
import numpy as np
m=int(input('enter m '))
n=int(input('enter n '))
mat=[]
#mat is a m*n matrix
for i in range (m) :
    r=[]
    for j in range (n) :
        x=int(input(f'enter a number for row {i} and column {j} '))
        r.append(x)
    mat.append(r)
a=[]
#every sentence of a is sum of a column
for i in range (n) :
    s=0
    for j in range (m) :
        s += mat[j][i]
    a.append(s)
b=[]
#every sentence of b is sum of a row
for i in range (m):
    s=0
    for j in range (n):
        s += mat[i][j]
    b.append(s)
print((np.array(mat)))
print(f'\n{a}\n{b}')
