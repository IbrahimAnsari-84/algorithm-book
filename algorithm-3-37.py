#this is to sum or multiplication to matrixes
import numpy as np
#this is to sum two matrixes
n=int(input('enter n '))
m=int(input('enter m '))
a=[]
for i in range(n):
    r=[]
    for j in range(m):
        r.append(int(input(f'enter number for row{i} and column{j} for first matrix')))
    a.append(r)
b=[]
for i in range(n):
    r=[]
    for j in range(m):
        r.append(int(input(f'enter number for row{i} and column{j} for second matrix')))
    b.append(r)
what_to_do=input('what to do?\nto sum, enter "+"\nto subtraction, enter "-" ')
if what_to_do == "sum" :
    c=[]
    for i in range(n):
        r=[]
        for j in range(m):
            r.append(a[i][j]+b[i][j])
        c.append(r)
    print(np.array(a))
    print(np.array(b))
    print(np.array(c))
elif what_to_do == "-" :
    c=[]
    for i in range(n):
        r=[]
        for j in range(m):
            r.append(a[i][j]-b[i][j])
        c.append(r)
    print(np.array(a))
    print(np.array(b))
    print(np.array(c))
