#this will change matrix[i][j] with matrix[j][i] (prints reverse of a matrix
import numpy as np
m=int(input('enter m '))
n=int(input('enter n '))
mat=[]
for i in range (m):
    r=[]
    for j in range (n):
        r.append(int(input(f'enter a number for row {i} and column {j} ')))
    mat.append(r)
new_mat=[]
for i in range (n) :
    r=[]
    for j in range (m) :
        r.append(mat[j][i])
    new_mat.append(r)
print(np.array(mat))
print(np.array(new_mat))
